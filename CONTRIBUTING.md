import pymongo

# Establece conexion con servidor MongoDB
miconet = pymongo.MongoClient("mongodb://localhost:27017/")

# Valida si la base de datos existe
dblist = miconet.list_database_names()
if "SLANGDB" in dblist:
    print("La base de datos existe")
else:
    print("No existe la base de datos")

# Base de datos
db = miconet["SLANGDB"]
# Coleccion
col = db["tbpalabras"]

# Insertar de un solo registro
registro = {"palabra": "xopa", "significa": "Que paso bro"}
graba = col.insert_one(registro)
print(graba.acknowledged)

# Buscar el primer registro
charly= {"palabra": "chuzo"}
ver = col.find_one(charly)
print(ver)
pass

# Insertar varios registros
registros = [{"palabra": "yumbo", "significa": "algo muy grande"},
             {"palabra": "cocoa", "significa": "habladurias"},
             {"palabra": "pipa", "significa": "se refiere al coco"},
             {"palabra": "vaina", "significa": "cualquier cosa"},
             {"palabra": "chuzo", "significa": "paso algo"}]
grabar = col.insert_many(registros)
print(grabar.acknowledged)

# Mostrar los registros de la tabla
for campos in col.find():
    print(campos)
pass

# Borrar un registro
borrauno = {"palabra": "vaina" }
col.delete_one(borrauno)
pass
# Mostrar los registros de la tabla
for campos in col.find():
    print(campos)
pass

# Borrar un registro
borrauno = {"significa": "habladurias" }
col.delete_one(borrauno)
pass

# Mostrar los registros de la tabla
for campos in col.find():
    print(campos)
pass

# Borrar todos los registros
borratodo = col.delete_many({})
print(str(borratodo.deleted_count) +  " registros eliminados.")
pass

# Eliminar la coleccion
col.drop()
print("coleccion eliminada.")
